%{
  #include <stdlib.h>
  #include <ctype.h>
  #include <string.h>
  #include "y.tab.h"
  
  int lineno = 1, colno = 1, tmp_lineno, tmp_colno;
%}

ID          [a-zA-Z_][_a-zA-Z0-9]*
CHAR        [^\n\\\']|\\.

%x          COMMENT

%%

"&"                           colno += strlen(yytext); return AMP;
"&&"                          colno += strlen(yytext); return AND;
"="                           colno += strlen(yytext); return ASSIGN;
"*"                           colno += strlen(yytext); return AST;
","                           colno += strlen(yytext); return COMMA;
"/"                           colno += strlen(yytext); return DIV;
"=="                          colno += strlen(yytext); return EQ;
">="                          colno += strlen(yytext); return GE;
">"                           colno += strlen(yytext); return GT;
"{"                           colno += strlen(yytext); return LBRACE;
"<="                          colno += strlen(yytext); return LE;
"("                           colno += strlen(yytext); return LPAR;
"["                           colno += strlen(yytext); return LSQ;
"<"                           colno += strlen(yytext); return LT;
"-"                           colno += strlen(yytext); return MINUS;
"%"                           colno += strlen(yytext); return MOD;
"!="                          colno += strlen(yytext); return NE;
"!"                           colno += strlen(yytext); return NOT;
"||"                          colno += strlen(yytext); return OR;
"+"                           colno += strlen(yytext); return PLUS;
"}"                           colno += strlen(yytext); return RBRACE;
")"                           colno += strlen(yytext); return RPAR;
"]"                           colno += strlen(yytext); return RSQ;
";"                           colno += strlen(yytext); return SEMI;
atoi                          colno += strlen(yytext); return ATOI;
char                          colno += strlen(yytext); return CHAR;
else                          colno += strlen(yytext); return ELSE;
if                            colno += strlen(yytext); return IF;
int                           colno += strlen(yytext); return INT;
itoa                          colno += strlen(yytext); return ITOA;
printf                        colno += strlen(yytext); return PRINTF;
return                        colno += strlen(yytext); return RETURN;
while                         colno += strlen(yytext); return WHILE;

auto|break|case|const|continue|default|do|double|enum|extern|float|for|goto|long|register|short|signed|sizeof|static|struct|switch|typedef|union|unsigned|void|volatile|"++"|"--"         colno += strlen(yytext); return RESERVED;


{ID}                        yylval.id=(char *)strdup(yytext);  colno += strlen(yytext);		return ID;
([1-9][0-9]*)|0             yylval.intlit=yytext;              colno += strlen(yytext);		return INTLIT;


\'{CHAR}?\'					yylval.chrlit=(char *)strdup(yytext); colno += strlen(yytext); return CHRLIT;
\'{CHAR}*\n					printf("Line %d, col %d: unterminated char constant\n", lineno, colno); colno = 1; lineno++;
\'{CHAR}*\\?				printf("Line %d, col %d: unterminated char constant\n", lineno, colno); colno += strlen(yytext);
\'{CHAR}{CHAR}+\' 			printf("Line %d, col %d: multi-character char constant\n", lineno, colno); colno += strlen(yytext);

\"([^\\\"\n]|\\.)*\"        yylval.strlit=(char *)strdup(yytext); colno += strlen(yytext); return STRLIT;
\"([^\\\"\n]|\\.)*\\?       printf("Line %d, col %d: unterminated string constant\n", lineno, colno); colno += strlen(yytext);

"/*"                        BEGIN COMMENT; tmp_lineno = lineno; tmp_colno = colno; colno+= 2;
<COMMENT>"*/"               BEGIN 0; colno += 2;
<COMMENT>.                  colno++;
<COMMENT>\n                 lineno++; colno=1;
<COMMENT><<EOF>>            printf("Line %d, col %d: unterminated comment\n", tmp_lineno, tmp_colno); yyterminate();

<<EOF>>                     return 0;

[ \t]                       colno++; /* skip whitespace */
\n                          lineno++; colno=1; /* skip whitespace */
.                           printf("Line %d, col %d: illegal character ('%s')\n", lineno, colno, yytext); colno++;

%%

int yywrap(void) 
{
  return 1;
}


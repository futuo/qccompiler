#ifndef AST_H
#define AST_H

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

/*the type of ast node*/
typedef enum { PROGRAM, DECLARATION, FUNCTION, STATEMENT, 
               OPERATOR, TERMINAL, DECLARATOR
             } nodetype;

/*Operadores*/
typedef enum { IS_OR, IS_AND, IS_EQ, IS_NE, IS_LT, IS_GT, IS_LE, IS_GE, IS_ADD,
               IS_SUB, IS_MUL, IS_DIV, IS_MOD, IS_NOT, IS_MINUS, IS_PLUS,
               IS_ADDR, IS_DEREF, IS_STORE, IS_CALL, IS_PRINT, IS_ATOI, IS_ITOA
             } operationtype;

/*Terminais*/
typedef enum { IS_INT, IS_CHAR, IS_POINTER, IS_ID, IS_INTLIT, IS_CHRLIT, IS_STRLIT 
             } terminaltype;

/*Statements*/
typedef enum { IS_SIMPLESTAT, IS_COMPOUNDSTAT, IS_IFELSE, IS_WHILE, IS_RETURN
             } statementtype;

/*Declaração/definição de funções*/
typedef enum { IS_FUNCDECLARATION, IS_FUNCDEFINITION, 
               IS_FUNCDECLARATOR, IS_FUNCBODY, IS_PARAMDECLARATION
             } functiontype;

/*Declaração de variáveis*/
typedef enum{ IS_SINGLE , IS_ARRAY
            } declaratortype;

/*the start node*/
typedef struct { 
    struct astnode *children;
}programNode;

/*operators*/
typedef struct { 
    operationtype type;
    struct astnode *children;
}operationNode;

/*terminals*/
typedef struct { 
    terminaltype type;
    char* value;
}terminalNode;

/*statements*/
typedef struct { 
    /*type of statement*/
    statementtype type;
    struct astnode *children;
}statementNode;

/*function definition/declaration*/
typedef struct { 
    functiontype type;
    struct astnode *children;
}functionNode;


/*declarations*/
typedef struct { 
    struct astnode *children;
}declarationNode;

/*declarator type*/
typedef struct {
    declaratortype type;
    struct astnode *children;
} declaratorNode;

/*tree node*/
typedef struct astnode {
  nodetype type;
  /* union must be last entry in the struct because of dynamic increases in sizes */
  union {
    programNode program;
    operationNode operation;
    terminalNode terminal;
    statementNode statement;
    functionNode function;
    declarationNode declaration;
    declaratorNode declarator;
  };
  struct astnode * next;
} node;



void insert( node *n1, node *n2 );
node* createStatementNode( statementtype type, ... );
node* createDeclarationNode( node* type_specifier, node* declarator_list );
node* createDeclaratorNode( declaratortype type, ... );
node* createTerminalNode(terminaltype type, char* value);
node* createOperationNode(operationtype type, ...);
node* createFunctionNode(functiontype type, ...);
node* appendNode(node* n1, node* n2);

void printProgramNode ( programNode* program, int depth );
void printOperationNode ( operationNode* operation, int depth );
void printTerminalNode ( terminalNode* terminal, int depth );
void printStatementNode ( statementNode* statement, int depth );
void printFunctionNode ( functionNode* func, int depth );
void printDeclarationNode ( declarationNode* declaration, int depth );
void printDeclaratorNode ( declaratorNode* declarator, int depth );
void printNode ( node* n, int depth );

void printCompoundStat( statementNode* statement, int depth );
void printIfElseNode( statementNode* statement, int depth );
void printWhileNode ( statementNode* statement, int depth );


#endif

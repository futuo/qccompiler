CLEAN_FILES =$(ZIP_NAME).zip $(PROG)

#confs do GCC e nome final do programa
CC =gcc
CC_FLAGS = -ly -Wall -g
CC_TMP_FILES =lex.yy.c y.tab.c
CC_INPUT_FILES = ast.h ast.c symtab.c symtab.h semantics.* compiler.*
PROG =qccompiler

#comando do yacc, ficheiro com código yacc
YACC =yacc
YACC_FLAGS =-d -v
YACC_FILE =$(PROG).y

#comando do lex, ficheiro com código lex
LEX =flex
LEX_FLAGS =
LEX_FILE =$(PROG).l

#confs do zip: nome do zip e ficheiros a incluir no zip
ZIP_NAME =$(PROG)
ZIP_FILES=$(LEX_FILE) $(YACC_FILE) $(CC_INPUT_FILES) frame.h


all: compile	

compile: lex yacc
	$(CC) $(CC_INPUT_FILES) $(CC_TMP_FILES) $(CC_FLAGS) -o $(PROG)
	rm $(CC_TMP_FILES) y.output

yacc:
	$(YACC) $(YACC_FLAGS) $(YACC_FILE)

lex:
	$(LEX) $(LEX_FLAGS) $(LEX_FILE)

zip: compile
	zip $(ZIP_NAME) $(ZIP_FILES)

clean:
	rm $(CLEAN_FILES)



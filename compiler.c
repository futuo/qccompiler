
#include "compiler.h"

#define max(a, b) ((a) > (b) ? (a) : (b))

int returncounter = 0;
extern environment* main_env;
int nR = 0;
int nIf = 0;
int nWhile = 0;
int nAnd = 0;
int nOr = 0;

int compile(FILE *dest, node *ast){

	printHeader(dest);
	printProgram(dest, ast, main_env);
	printFooter(dest);

	return 0;

}

void printHeader(FILE *dest){

	fprintf(dest, "#include \"frame.h\"\n");
	fprintf(dest, "#include <stdlib.h>\n");
	fprintf(dest, "#include <stdio.h>\n\n");

	fprintf(dest, "int main(int argc, char **argv){\n");
	fprintf(dest, "int _ra = 0;\n");
	fprintf(dest, "long int _rv = 0;\n");
	fprintf(dest, "frame* fp=NULL;\n");
	fprintf(dest, "frame* sp=NULL;\n\n");

}

void printFooter(FILE *dest){
	printRedirector(dest);

	fprintf(dest, "return 0;\n}\n\n/*Agora e que vai carago !*/\n\n");
}

void printProgram(FILE *dest, node *ast, environment *pe){

	if(ast){

		node *aux;
		switch( ast->type ){
			case PROGRAM    :
				for( aux = ast->program.children->next; aux != NULL; aux = aux->next )
					printProgram(dest, aux, pe);
			break;

			case DECLARATION:
				printDeclarations( dest, ast, pe);
			break;

			case FUNCTION   :
				switch( ast->function.type )
				{
					case IS_FUNCDEFINITION: printFunction( dest, ast, pe ); break;
					default: break;
				}
			break;

			case STATEMENT  :
				printStatement(dest, ast, pe);
			break;

			case OPERATOR   :
			case TERMINAL   :
				printOperation(dest, ast, pe);
			break;

			default: break;
		}
	}
}

void printStatement(FILE *dest, node *ast, environment *e){

	if(ast){
	
		node *aux;
		switch( ast->statement.type ){

			case IS_SIMPLESTAT:
			case IS_COMPOUNDSTAT: 
				for(aux = ast->statement.children->next; aux!=NULL; aux=aux->next)
					printProgram(dest, aux, e);
			break;

			case IS_IFELSE: printIfElse(dest, ast, e); break;
			case IS_WHILE: printWhile(dest, ast, e); break;
			case IS_RETURN: printReturn(dest, ast, e); break;

		}
	}
}

void printReturn( FILE *dest, node *ast, environment *e ){

	node *expr = ast->statement.children->next;
	int type, npointer, lvalue;

	int expr_r = printExpression( dest, expr, e, &type, &npointer, &lvalue, 0 );

	fprintf(dest, "_rv = (long int)((%s)(sp->R[%d]));\n", termtypeToStr(type, npointer), expr_r );
    fprintf(dest, "_ra=sp->return_address;\n");
    fprintf(dest, "sp=sp->parent;\n");
    fprintf(dest, "if(sp != NULL)\tfp=sp->parent;\n");
    fprintf(dest, "goto redirector;\n");

}

void printWhile( FILE *dest, node* whileStat, environment *e ){

	node* expression = whileStat->statement.children->next;
 	node* w_statement= expression->next;

 	int label_num = nWhile++;

 	fprintf( dest, "\nwhile%d:\n", label_num);

 	//imprime a expression
 	int expr_r = printOperation( dest, expression, e );

 	fprintf(dest, "if( !sp->R[%d] )\tgoto endwhile%d;\n", expr_r, label_num);

 	printProgram( dest, w_statement, e );

 	fprintf(dest, "goto while%d;\nendwhile%d:\n", label_num, label_num);

}

void printIfElse( FILE *dest, node* ifElseStat, environment *e ){

 	node* expression = ifElseStat->statement.children->next;
 	node* ifstatement= expression->next;
 	
 	int label_num = nIf++;

 	//imprime a expression
 	int expr_r = printOperation( dest, expression, e );

 	fprintf(dest, "\nif( !sp->R[%d] )\tgoto else%d;\n", expr_r, label_num);

 	printProgram( dest, ifstatement, e );

 	fprintf(dest, "goto endif%d;\n\nelse%d:\n", label_num, label_num);

 	if ( ifstatement->next != NULL ){
 		//se tem else faz print dos stats do else
	 	printProgram( dest, ifstatement->next, e );
 	}

 	fprintf(dest, "endif%d:\n", label_num);

}

int printOperation(FILE *dest, node *ast, environment*e){
	int type, npointer, is_lvalue ;
	return printExpression(dest, ast, e, &type, &npointer, &is_lvalue, 0);
}

char* getSymId( char *id, environment *e, int *type, int *npointer ){
	symbol *s;
	char *buff = (char *)calloc(128, sizeof(char));

	if ( (s = searchSym( id, e )) == NULL ){
		s = searchSym( id, main_env);
		if(s){
			sprintf(buff, "(*(%s) (globals[%d]))",
					symtypeToStr(s->type, s->npointer+1+(s->narray>0)),
					s->offset	);

			*type = symtypeToTermtype(s->type);
			*npointer = s->npointer + (s->narray>0);
		}
		else{
			printf("This should not happen symbol %s must exist on the symbol table\n", id);
			exit(0);
		}
	}
	else{
		sprintf(buff, "(*(%s) (sp->locals[%d]))",
				symtypeToStr(s->type, s->npointer+1+(s->narray>0)),
				s->offset	);

		*type = symtypeToTermtype(s->type);
		*npointer = s->npointer + (s->narray>0);		
	}


	return buff;
}

void printMain( FILE *dest, node *ast, environment *e ){
	char *fid = "main";        
			
	fprintf(dest, "\n/*########## BEGIN OF MAIN BLOCK ##########*/\n");
	fprintf(dest, "fp = sp;\n");
	fprintf(dest, "sp = (frame *)malloc(sizeof(frame));\n");
	fprintf(dest, "sp->parent = fp;\n");
	fprintf(dest, "sp->return_address=-1;\n");
	
	//function declarator
	environment *fenv = searchEnvironment( e, fid );
	
	//declaracao dos parametros
	symbol *i = fenv->symtab->next->next;//argc
	fprintf(dest, "sp->locals[%d] = &argc;\n", i->offset);
	i = i->next;    //argv
	fprintf(dest, "sp->locals[%d] = &argv;\n\n", i->offset);

	node *funcbody = ast->function.children->next->next->next,
		 *aux;

	for( aux = funcbody->function.children->next; aux; aux=aux->next )
		printProgram( dest, aux, fenv );
		
	//epilogo
	fprintf(dest, "\n/*########## END OF MAIN BLOCK ##########*/\n\n");
}

void printPrintf(FILE *dest, node *ast, environment *e, int *type, int *npointer, int *is_lvalue, int give_me_ref){
	
	node *toprint = ast->operation.children->next;
	int type1, npointer1;
	int r1 = printExpression(dest, toprint, e, &type1, &npointer1, is_lvalue, 0);

	fprintf(dest, "printf(\"%%s\", (%s)sp->R[%d]);\n", termtypeToStr(type1, npointer1), r1);

	*type = IS_INTLIT;
	*npointer = 0;

}

int printItoaOperation(FILE *dest, node *ast, environment *e, int *type, int *npointer, int *is_lvalue, int give_me_ref){
	node *buffer = ast->operation.children->next->next;
	node *toconvert = ast->operation.children->next;
	int type1, type2, npointer1, npointer2, is_lvalue1, is_lvalue2;
	
	int r1 = printExpression(dest, buffer, e, &type1, &npointer1, &is_lvalue1, 0);
	int r2 = printExpression(dest, toconvert, e, &type2, &npointer2, &is_lvalue2, 0);

	fprintf(dest, "sprintf((%s)sp->R[%d], \"%%d\" ,(%s)sp->R[%d]);\n",
						termtypeToStr(type1, npointer1),
						r1,
						termtypeToStr(type2, npointer2),
						r2
						);

	*type = IS_CHRLIT;
	*npointer = 1;
	*is_lvalue = 0;


	return r1;
}

int printAtoiOperation(FILE *dest, node *ast, environment *e, int *type, int *npointer, int *is_lvalue, int give_me_ref){
	int r;
	node *toconvert = ast->operation.children->next;
	int type1, npointer1, is_lvalue1;
	int r1 = printExpression(dest, toconvert, e, &type1, &npointer1, &is_lvalue1, 0);
	r = nR++;

	fprintf(dest, "sscanf((%s)sp->R[%d], \"%%d\", &sp->R[%d]);\n", 
						termtypeToStr(type1, npointer1),
						r1,
						r
				);

	*type = IS_INTLIT;
	*npointer = 0;
	*is_lvalue = 0;

	

	return r;
}

int printExpression(FILE *dest, node *exp, environment*e, int *type, int *npointer, int *is_lvalue, int give_me_ref){
	// condicao de pargem
	if( exp->type == TERMINAL ){
		if(exp->terminal.type == IS_ID){
			return loadID(dest, exp, e, type, npointer, is_lvalue, give_me_ref);
		}
		else{
			return loadLit(dest, exp, e, type, npointer, is_lvalue, give_me_ref);
		}
	}
	else if (exp->type == OPERATOR ){
		switch( exp->operation.type ){
            
            case IS_ADD 	: return printAddSub(dest, exp, e, type, npointer, '+', is_lvalue, give_me_ref);
            case IS_SUB 	: return printAddSub(dest, exp, e, type, npointer, '-', is_lvalue, give_me_ref);

			case IS_DIV 	: return printBinary(dest, exp, e, "/", type, npointer, is_lvalue, give_me_ref);
			case IS_MUL 	: return printBinary(dest, exp, e, "*", type, npointer, is_lvalue, give_me_ref);
			case IS_MOD 	: return printBinary(dest, exp, e, "%", type, npointer, is_lvalue, give_me_ref);
			case IS_OR 		: return printOr 	(dest, exp, e, "||", type, npointer, is_lvalue, give_me_ref);
			case IS_AND 	: return printAnd 	(dest, exp, e, "&&", type, npointer, is_lvalue, give_me_ref);
			case IS_EQ 		: return printBinary(dest, exp, e, "==", type, npointer, is_lvalue, give_me_ref);
			case IS_NE 		: return printBinary(dest, exp, e, "!=", type, npointer, is_lvalue, give_me_ref);
			case IS_LT 		: return printBinary(dest, exp, e, "<", type, npointer, is_lvalue, give_me_ref);
			case IS_GT 		: return printBinary(dest, exp, e, ">", type, npointer, is_lvalue, give_me_ref);
			case IS_LE    	: return printBinary(dest, exp, e, "<=", type, npointer, is_lvalue, give_me_ref);
			case IS_GE    	: return printBinary(dest, exp, e, ">=", type, npointer, is_lvalue, give_me_ref);
			case IS_ITOA  	: return printItoaOperation(dest, exp, e, type, npointer, is_lvalue, give_me_ref);
			case IS_PRINT 	: printPrintf(dest, exp, e, type, npointer, is_lvalue, give_me_ref); break;
			case IS_ATOI  	: return printAtoiOperation(dest, exp, e, type, npointer, is_lvalue, give_me_ref);

			case IS_NOT   	: return printNot(dest, exp, e, type, npointer, is_lvalue, give_me_ref);
			case IS_MINUS 	: return printUnary(dest, exp, e, type, npointer, '-', is_lvalue, give_me_ref);
			case IS_PLUS  	: return printUnary(dest, exp, e, type, npointer, '+', is_lvalue, give_me_ref);

			case IS_DEREF 	: return printDeref(dest, exp, e, type, npointer, is_lvalue, give_me_ref);
			case IS_ADDR  	: return printAddr(dest, exp, e, type, npointer, is_lvalue, give_me_ref);
			case IS_STORE 	: return printStore(dest, exp, e, type, npointer, is_lvalue, give_me_ref);
			case IS_CALL  	: return printFunctionCall(dest, exp, e, type, npointer );

			default: break;

		}
	}
	return 0;
}

int loadLit(FILE *dest, node *ast, environment *e, int *type, int *npointer, int *is_lvalue, int give_me_ref){
	int r = nR++;
	fprintf(dest, "sp->R[%d] = (long int) (%s);\n", r, ast->terminal.value);	

	if(ast->terminal.type == IS_STRLIT){
		*type = IS_CHRLIT;
		*npointer = 1;
	}
	else{
		*type = IS_INTLIT;
		*npointer = 0;
	}
	*is_lvalue = 0;

	

	return r;
}

int loadID(FILE *dest, node *ast, environment *e, int *type, int *npointer, int *is_lvalue, int give_me_ref){
	int r = nR++;
	char *id = ast->terminal.value;
	symbol *s = searchSym(id, e);
	if(!s && !(s=searchSym(id, main_env)) ){
		printf("This should not happen\n");
		exit(0);
	}
	char *buff = getSymId(id, e, type, npointer);

	if(give_me_ref){
		fprintf(dest, "sp->R[%d] = (long int) (&(%s));\n", r, buff);		
		(*npointer)++;
  	}	
	else{
		fprintf(dest, "sp->R[%d] = (long int) (%s);\n", r, buff);		
	}

	free(buff);
	*is_lvalue = 1;
	return r;
}

int printAnd(FILE *dest, node *ast, environment *e, char *op, int *type, int *npointer, int *is_lvalue, int give_me_ref){
	
	int r;
	int label_num = nAnd++;
	node *n1 = ast->operation.children->next;
	node *n2 = ast->operation.children->next->next;
	int type1, type2, npointer1, npointer2, is_lvalue1, is_lvalue2;

	int r1 = printExpression(dest, n1, e, &type1, &npointer1, &is_lvalue1, 0);
	fprintf( dest, "if( !sp->R[%d] )\tgoto end_and%d;\n", r1, label_num );
	int r2 = printExpression(dest, n2, e, &type2, &npointer2, &is_lvalue2, 0);
	fprintf( dest, "end_and%d:\n", label_num );

	r = nR++;
	
	*type = IS_INTLIT;
	*npointer = 0;
	*is_lvalue = 0;

	fprintf(dest, "sp->R[%d] = (long int) (sp->R[%d] %s sp->R[%d]);\n", r, r1, op, r2);
	
	return r;
}

int printOr(FILE *dest, node *ast, environment *e, char *op, int *type, int *npointer, int *is_lvalue, int give_me_ref){
	
	int r;
	int label_num = nOr++;
	node *n1 = ast->operation.children->next;
	node *n2 = ast->operation.children->next->next;
	int type1, type2, npointer1, npointer2, is_lvalue1, is_lvalue2;

	int r1 = printExpression(dest, n1, e, &type1, &npointer1, &is_lvalue1, 0);
	fprintf( dest, "if( sp->R[%d] )\tgoto end_or%d;\n", r1, label_num );
	int r2 = printExpression(dest, n2, e, &type2, &npointer2, &is_lvalue2, 0);
	fprintf( dest, "end_or%d:\n", label_num );

	r = nR++;
	
	*type = IS_INTLIT;
	*npointer = 0;
	*is_lvalue = 0;

	fprintf(dest, "sp->R[%d] = (long int) (sp->R[%d] %s sp->R[%d]);\n", r, r1, op, r2);
	
	return r;
}


int printBinary(FILE *dest, node *ast, environment *e, char *op, int *type, int *npointer, int *is_lvalue, int give_me_ref){
	int r;
	node *n1 = ast->operation.children->next;
	node *n2 = ast->operation.children->next->next;
	int type1, type2, npointer1, npointer2, is_lvalue1, is_lvalue2;

	int r1 = printExpression(dest, n1, e, &type1, &npointer1, &is_lvalue1, 0);
	int r2 = printExpression(dest, n2, e, &type2, &npointer2, &is_lvalue2, 0);
	r = nR++;
	
	*type = IS_INTLIT;
	*npointer = 0;
	*is_lvalue = 0;

	fprintf(dest, "sp->R[%d] = (long int) (sp->R[%d] %s sp->R[%d]);\n", r, r1, op, r2);
	
	return r;
}

int printAddSub(FILE *dest, node *ast, environment *e, int *type, int *npointer , char op, int *is_lvalue, int give_me_ref){	
    int r;
    node *n1 = ast->operation.children->next;
    node *n2 = ast->operation.children->next->next;
    int type1, type2, npointer1, npointer2, is_lvalue1, is_lvalue2;

    int r1 = printExpression(dest, n1, e, &type1, &npointer1, &is_lvalue1, 0);
    int r2 = printExpression(dest, n2, e, &type2, &npointer2, &is_lvalue2, 0);
    r = nR++;

    fprintf(dest, "sp->R[%d] = (long int) (((%s)sp->R[%d]) %c ((%s)sp->R[%d]));\n", 
                        r,
                        termtypeToStr(type1, npointer1),
                        r1,
                        op,
                        termtypeToStr(type2, npointer2),
                        r2);

    if(npointer1 == npointer2)
    	*type = IS_INTLIT;
    else	
	    *type = npointer1 > npointer2 ? type1 : type2;
    
    *npointer = abs(npointer1 - npointer2);
    *is_lvalue = 0;

	

    return r;
}

int printUnary(FILE *dest, node *ast, environment *e, int *type, int *npointer, char op, int *is_lvalue, int give_me_ref){
    int r;
    node *n1 = ast->operation.children->next;

    int r1 = printExpression(dest, n1, e, type, npointer, is_lvalue, 0);
    r = nR++;

    fprintf(dest, "sp->R[%d] = (long int) (%c ( (%s)sp->R[%d] ));\n",
    				r,
    				op,
    				termtypeToStr(*type, *npointer),
    				r1  );
	

    *is_lvalue = 0;
    
    return r;
}

int printNot(FILE *dest, node *ast, environment *e, int *type, int *npointer, int *is_lvalue, int give_me_ref){
    int r;
    node *n1 = ast->operation.children->next;

    int r1 = printExpression(dest, n1, e, type, npointer, is_lvalue, 0);
    r = nR++;

    fprintf(dest, "sp->R[%d] = (long int) (! ( (%s)sp->R[%d] ));\n",
    				r,
    				termtypeToStr(*type, *npointer),
    				r1  );
	

    *type = IS_INTLIT;
    *npointer = 0;
    *is_lvalue = 0;

    return r;
}

int printDeref(FILE *dest, node *ast, environment *e, int *type, int *npointer, int *is_lvalue, int give_me_ref){
    int r;
    node *n1 = ast->operation.children->next;

    int r1 = printExpression(dest, n1, e, type, npointer, is_lvalue, 0);
    r = nR++;

	if(give_me_ref){
		fprintf(dest, "sp->R[%d] = (long int) (& (* ( (%s)sp->R[%d] )));\n",
						r,
						termtypeToStr(*type, *npointer),
						r1  );
	}
	else{
	    fprintf(dest, "sp->R[%d] = (long int) (* ( (%s)sp->R[%d] ));\n",
	    				r,
	    				termtypeToStr(*type, *npointer),
	    				r1  );		
	    (*npointer)--;
	}

    *is_lvalue = 1;

    return r;
}

int printAddr(FILE *dest, node *ast, environment *e, int *type, int *npointer, int *is_lvalue, int give_me_ref){
	
    node *n1 = ast->operation.children->next;
    int type1, npointer1, is_lvalue1;

    int r1 = printExpression(dest, n1, e, &type1, &npointer1, &is_lvalue1, 1);

    *type = type1;
    *npointer = npointer1+1;
    *is_lvalue = 0;

    return r1;

}

int printStore(FILE *dest, node *ast, environment *e, int *type, int *npointer, int *is_lvalue, int give_me_ref){
    int r;
    node *n1 = ast->operation.children->next;
    node *n2 = ast->operation.children->next->next;
    int type1, type2, npointer1, npointer2, is_lvalue1, is_lvalue2;

    int r1 = printExpression(dest, n1, e, &type1, &npointer1, &is_lvalue1, 1);
    int r2 = printExpression(dest, n2, e, &type2, &npointer2, &is_lvalue2, 0);
    r = nR++;

    fprintf(dest, "(*(%s)(sp->R[%d])) = (%s)(sp->R[%d]);\n",
    				termtypeToStr(type1, npointer1),
    				r1,
    				termtypeToStr(type2, npointer2),
    				r2
    				);

    *type = type1;
    *npointer = npointer1-1;

    if(give_me_ref){
    	fprintf(dest, "sp->R[%d] = (long int) ((%s)(sp->R[%d]));\n",
    				r,
    				termtypeToStr(type1, npointer1),
    				r1
    			);
    	(*npointer)++;
    }
    else{
    	fprintf(dest, "sp->R[%d] = (long int) ((%s)(sp->R[%d]));\n",
    				r,
    				termtypeToStr(type2, npointer2),
    				r2
    			);    	
    }


    *is_lvalue = 1;
    return r;
}

void printFunction( FILE *dest, node *ast, environment *e ){

    node *id_node = ast->function.children->next->next->function.children->next;
    while( id_node->terminal.type != IS_ID )
    	id_node = id_node->next;
    char *fid = id_node->terminal.value;

	if(strcmp(fid,"main")==0){
		printMain(dest, ast, e);
		return;
	}


    fprintf(dest, "\ngoto %sskip;\n", fid );
    fprintf(dest, "/*########## BEGIN OF %s FUNC ##########*/\n", fid);
    fprintf(dest, "%s:\n", fid );
    
    fprintf(dest, "fp = sp;\n");
    fprintf(dest, "sp = (frame *)malloc(sizeof(frame));\n");
    fprintf(dest, "sp->parent = fp;\n");
    fprintf(dest, "sp->return_address=_ra;\n");

    //function declarator
    environment *fenv = searchEnvironment( e, fid );
    symbol *i;
    //declaracao dos parametros
    for( i = fenv->symtab->next->next; i && i->is_param; i = i->next ){
        fprintf(dest, "/*declaracao do parametro %s*/\n", i->id);
        fprintf(dest, "sp->locals[%d] = fp->outgoing[%d];\n", i->offset, i->offset);
    }

    node *funcbody = ast->function.children->next->next->next,
         *aux;
    for( aux = funcbody->function.children->next; aux; aux=aux->next ){
        printProgram( dest, aux, fenv );
    }
        
    fprintf(dest, "_ra=sp->return_address;\n");
    fprintf(dest, "sp=sp->parent;\n");
    fprintf(dest, "fp=sp->parent;\n");
    fprintf(dest, "goto redirector;\n");
    fprintf(dest, "/*########## END OF %s FUNC ##########*/\n", fid);
    fprintf(dest, "%sskip:\n\n", fid);

}

int printFunctionCall(FILE *dest, node *ast, environment *e, int *type_p, int *npointer_p){

	int ra = returncounter++, r, fc_r;
    char *fid = ast->function.children->next->terminal.value;
    node *it = ast->function.children->next->next;
    int type, npointer, is_lvalue, i;


    environment *fe = searchEnvironment(main_env, fid);

    // guardar registos e tipos dos parametros (evitar overwrites nos outgoing)
    int* params_r = (int *)malloc(sizeof(int)*(fe->nparams));
    int* types = (int *)malloc(sizeof(int)*(fe->nparams));
    int* npointers = (int *)malloc(sizeof(int)*(fe->nparams));    

    
    symbol *s = fe->symtab->next;

    *type_p = symtypeToTermtype( s->type );    
    *npointer_p = s->npointer;
    
    int cnt = 0;
    for(i=0; it ; it = it->next, i++){
    	params_r[i] = printExpression(dest, it, e, &types[i], &npointers[i], &is_lvalue, 0);
    }
    for(i = 0; i< fe->nparams; i++){    	
		fprintf(dest, "sp->outgoing[%d] = malloc(sizeof(%s));\n", i, termtypeToStr(types[i], npointers[i]));
		fprintf(dest, "*(%s)(sp->outgoing[%d]) = (%s)(sp->R[%d]);\n", termtypeToStr(types[i], npointers[i]+1), i, termtypeToStr(types[i], npointers[i]), params_r[i]);
    }


    fc_r = nR++;

    fprintf(dest, "_ra = %d;\n", ra);
    fprintf(dest, "goto %s;\n", fid);
    fprintf(dest, "return%d:\n", ra);
    fprintf(dest, "sp->R[%d] = (long int)((%s)_rv);\n", fc_r, termtypeToStr(*type_p, *npointer_p) );

    return fc_r;
}

void printDeclarations(FILE *dest, node *ast, environment *e){
	node *aux = ast->declaration.children->next->next;
	for( ; aux != NULL; aux = aux->next )
		printVariableDeclaration(dest, aux, e);
}

//assumes ast is a declarator node
void printVariableDeclaration(FILE *dest, node *ast, environment *e){
	
	//vai buscar o id da variavel ao no da ast
	int npointer;
	char *vid;
	node *children = ast->declarator.children->next;

	// asteriscos
	for(; children->terminal.type != IS_ID; children = children->next, npointer++ );

	vid = children->terminal.value;

	//procura a variavel na tabela de simbolos
	symbol* s= searchSym(vid, e);
	if(s){
		fprintf( dest, "\n/*Declaracao da variavel %s */\n", vid );
		//declara a variavel consoante o tipo de dados;
		if( isGlobal(e) ){
			printDeclaration(dest, s, "globals");
		}else{
			printDeclaration(dest, s, "sp->locals");
		}
		fprintf(dest, "\n");
	}
	else{
		printf("This should not happen variable %s must exist in symbol table\n", vid);
		exit(0);
	}
}

void printDeclaration(FILE *dest, symbol *s, char *saveto){
	int npointer = s->npointer + (s->narray > 0) +1 ;
	char decl_str[512];

	sprintf(decl_str, "%s[%d] = (%s) malloc(sizeof(%s));", 
							saveto,
							s->offset,
							symtypeToStr(s->type, npointer),
							symtypeToStr(s->type, npointer-1)
			);

	fprintf(dest, "%s\n", decl_str);
	if(s->narray > 0){
		sprintf(decl_str, "*(%s)%s[%d] = (%s) malloc(sizeof(%s)*%d);",
					symtypeToStr(s->type, npointer),
					saveto,
					s->offset,
					symtypeToStr(s->type, npointer-1),
					symtypeToStr(s->type, npointer-2),
					s->narray
				);
		fprintf(dest, "%s\n", decl_str);
	}
}

void printRedirector(FILE* dest) {
	int i;
	fprintf(dest, "/*Redirector*/\n");
	fprintf(dest, "goto exit;\n");
	fprintf(dest, "redirector:\n");

	fprintf(dest, "if(_ra==-1) goto exit;\n");

	for(i=0; i<returncounter; i++)              //Para cada endereco de retorno, sua label associada
	{
		fprintf(dest, "if(_ra==%d) goto return%d;\n", i, i);
	}
	fprintf(dest, "exit:\n;\n");
}

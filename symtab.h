#ifndef SYMTAB_H
#define SYMTAB_H

#include <stdio.h>
#include <stdlib.h>
#include "ast.h"

typedef enum{	_int,
				_char,
				_function,
			} sym_type;

// linked list of symbols
typedef struct _s{
	char *id;
	
	sym_type type;
	int npointer;
	int narray;
	int offset;

	char is_param;
	char is_return;
	
	struct _s *next;
} symbol;


typedef struct _e{
	// in case of function environment
	int nparams;			// number of parameters
	char is_defined;		// if its already defined
	char used;				// if there is at least one call to the function

	char *id;
	int nvars;
	symbol* symtab;
	struct _e *envs;
	struct _e *next;
} environment;

int insertSym(environment *e, sym_type t, char *id, int npointer, int narray, char is_param, char is_return);
int insertFuncSym(environment *e, char *id);
int insertGlobalSym(environment *e, sym_type t, char *id, int npointer, int narray);
symbol *searchSym(char *id, environment *e);
environment* newEnvironment(char *id);
environment* searchEnvironment(environment* parent, char *env_id);
environment* insertEnvironment(environment* parent, char *env_id);
symbol* newSymbol(char *id, sym_type t, int npointer, int narray, char is_param, char is_return);
char *symtypeToStr(sym_type t, int npointers);
char *termtypeToStr(int t, int npointers);
void printSymtab(environment *e);

#endif

#include "ast.h"


void insert( node *n1, node *n2 ){
	node *ptr = n1->program.children;
	while(ptr->next != NULL){
		// printf("aksbdkasd\n");
		ptr = ptr->next;
	}
	// printf("%d\n", n2->type);
	ptr->next = n2;
}

node* createFunctionNode(functiontype type, ...){
	node* n = (node*)malloc(sizeof(node));
	n->type = FUNCTION;
	n->next = NULL;

	n->function.type = type;
	n->function.children = (node*)malloc(sizeof(node));
	n->function.children->next = NULL;

	node *addr, *aux = n->function.children;

	va_list args;
	va_start(args, type);

	while((addr = va_arg(args, node *)) != NULL){
		while(aux->next != NULL)
			aux = aux->next;
		aux->next = addr;
		aux = aux->next;
	}
	va_end(args);

	return n;
}

node* createStatementNode( statementtype type, ... ){

	node* n = (node*)malloc(sizeof(node));
	n->type = STATEMENT;
	n->next = NULL;

	n->statement.type = type;
	n->statement.children = (node*)malloc(sizeof(node));
	n->statement.children->next = NULL;

	node *addr, *aux = n->statement.children;

	va_list args;
	va_start(args, type);

	if( !(type == IS_IFELSE) ){
		while((addr = va_arg(args, node *)) != NULL){
			while(aux->next != NULL)
				aux = aux->next;
			aux->next = addr;
			aux = aux->next;
		}
	}else{
		int i;
		//lê a expression e os statements
		for( i=0; i<3; i++ ){
			addr = va_arg(args, node *);
			if( addr == NULL )
				addr = createStatementNode(IS_SIMPLESTAT, NULL);
			while(aux->next != NULL)
				aux = aux->next;
			aux->next = addr;
			aux = aux->next;
		}
	}

	va_end(args);

	return n;
}

node* createDeclarationNode( node* type_specifier, node* declarator_list ){

	node *n = (node*)malloc(sizeof(node));
	n->type = DECLARATION;
	n->next = NULL;

	node *children = (node*)malloc(sizeof(node));
	type_specifier->next = declarator_list;
	children->next = type_specifier;

	n->declaration.children = children;

	return n;
}

node* createDeclaratorNode( declaratortype type, ... ){
	node* n = (node*)malloc(sizeof(node));
	n->type = DECLARATOR;
	n->next = NULL;

	n->declarator.type = type;
	n->declarator.children = (node*)malloc(sizeof(node));
	n->declarator.children->next = NULL;

	node *addr, *aux = n->declarator.children;

	va_list args;
	va_start(args, type);

	while((addr = va_arg(args, node *)) != NULL){
		while(aux->next != NULL)
			aux = aux->next;
		aux->next = addr;
		aux = aux->next;
	}

	va_end(args);

	return n;

}

node* createTerminalNode(terminaltype type, char* value){

	node *n = (node *)malloc(sizeof(node));
	n->type = TERMINAL;
	n->next = NULL;

	n->terminal.type = type;
	n->terminal.value = NULL;

	if( value != NULL ){
		n->terminal.value = (char *)malloc(sizeof(char) * (strlen(value)+1) );
		strcpy( n->terminal.value, value );
		if( type != IS_INTLIT )
			free(value);
	}

	return n;
}

/*creates an operation node and initializes it with N operands*/
node* createOperationNode(operationtype type, ...){

	node *n = (node *)malloc(sizeof(node));
	n->type = OPERATOR;
	n->next = NULL;

	n->operation.type = type;
	n->operation.children = (node*)malloc(sizeof(node));
	n->operation.children->next = NULL;

	va_list args;
	node *addr;
	node *aux = n->operation.children;

	va_start(args, type);

	while((addr = va_arg(args, node *)) != NULL){
		while(aux->next != NULL)
			aux = aux->next;
		aux->next = addr;
		aux = aux->next;
	}

	va_end(args);

	return n;

}

node* appendNode(node* n1, node* n2){
	
	if(n1 == NULL)
		return n2;

	node *aux = n1;
	while(aux->next != NULL)
		aux = aux->next;
	aux->next = n2;
	return n1;
}





/* functions used to print the ast tree */
void printNode ( node* n, int depth ){
	nodetype type = n->type;
	switch( type ){
		case PROGRAM 	:	printProgramNode 	( &(n->program)    , depth ); break;
		case DECLARATION: 	printDeclarationNode( &(n->declaration), depth ); break;
		case FUNCTION 	:	printFunctionNode 	( &(n->function) 	   , depth ); break;
		case STATEMENT  : 	printStatementNode 	( &(n->statement)  , depth ); break;
		case OPERATOR 	: 	printOperationNode 	( &(n->operation)  , depth ); break;
		case TERMINAL 	: 	printTerminalNode 	( &(n->terminal)   , depth ); break;
		case DECLARATOR : 	printDeclaratorNode ( &(n->declarator) , depth ); break;
	}
}

void printProgramNode ( programNode* program, int depth ){
	int i;
	for(i=0; i<depth; i++)
		printf("  ");
	printf("Program\n");

	node *aux = program->children->next;
	while(aux != NULL){
		printNode(aux, depth+1);
		aux = aux->next;
	}
}

void printOperationNode ( operationNode* operation, int depth ){
	int i;
	for(i=0; i<depth; i++)
		printf("  ");

	operationtype type = operation->type;
	switch(type){
		case IS_OR	 : printf("Or\n");    break;
		case IS_AND	 : printf("And\n");   break;
		case IS_EQ	 : printf("Eq\n");    break;
		case IS_NE	 : printf("Ne\n");    break;
		case IS_LT	 : printf("Lt\n");    break;
		case IS_GT	 : printf("Gt\n");    break;
		case IS_LE	 : printf("Le\n");    break;
		case IS_GE	 : printf("Ge\n");    break;
		case IS_ADD	 : printf("Add\n");   break;
		case IS_SUB	 : printf("Sub\n");   break;
		case IS_MUL	 : printf("Mul\n");   break;
		case IS_DIV	 : printf("Div\n");   break;
		case IS_MOD	 : printf("Mod\n");   break;
		case IS_NOT	 : printf("Not\n");   break;
		case IS_MINUS: printf("Minus\n"); break;
		case IS_PLUS : printf("Plus\n");  break;
		case IS_ADDR : printf("Addr\n");  break;
		case IS_DEREF: printf("Deref\n"); break;
		case IS_STORE: printf("Store\n"); break;
		case IS_CALL : printf("Call\n");  break;
		case IS_PRINT: printf("Print\n"); break;
		case IS_ATOI : printf("Atoi\n");  break;
		case IS_ITOA : printf("Itoa\n");  break;
	}

	node *aux = operation->children->next;
	while(aux != NULL){
		printNode(aux, depth+1);
		aux = aux->next;
	}
}

void printTerminalNode ( terminalNode* terminal, int depth ){
	int i;
	for(i=0; i<depth; i++)
		printf("  ");
	
	terminaltype type = terminal->type;
	switch(type){
		case IS_INT		: printf( "Int\n" ); break;
		case IS_CHAR	: printf( "Char\n" ); break;
		case IS_POINTER	: printf( "Pointer\n" ); break;
		case IS_ID		: printf( "Id(%s)\n"    , terminal->value ); break;
		case IS_INTLIT	: printf( "IntLit(%s)\n", terminal->value ); break;
		case IS_CHRLIT	: printf( "ChrLit(%s)\n", terminal->value ); break;
		case IS_STRLIT	: printf( "StrLit(%s)\n", terminal->value ); break;
	}
}

void printStatementNode ( statementNode* statement, int depth ){

	statementtype type = statement->type;

	if( type != IS_SIMPLESTAT && type != IS_COMPOUNDSTAT ){
		int i;
		for(i=0; i<depth; i++)
			printf("  ");
	}

	switch(type){
		case IS_SIMPLESTAT: depth -= 1; break;
		case IS_COMPOUNDSTAT: printCompoundStat(statement, depth); return;
		case IS_IFELSE: printf("IfElse\n"); printIfElseNode(statement, depth); return;
		case IS_WHILE: printf("While\n"); printWhileNode(statement, depth); return;
		case IS_RETURN: printf("Return\n"); break;
	}

	node *aux = statement->children->next;
	while(aux != NULL){
		printNode(aux, depth+1);
		aux = aux->next;
	}
}

void printCompoundStat( statementNode* statement, int depth ){

	int i;
	if( ! (statement->children->next == NULL || statement->children->next->next == NULL) ){
		for(i=0; i<depth; i++)
			printf("  ");

		depth=depth+1;
		printf("CompoundStat\n");
	}

	node *aux = statement->children->next;
	while(aux != NULL){
		printNode(aux, depth);
		aux = aux->next;
	}
}

void printIfElseNode( statementNode* statement, int depth ){

 	node* expression = statement->children->next;
 	node* ifstatement= expression->next;
 	
 	//imprime a expression
 	printNode( expression, depth+1 );

 	int i;

 	//caso seja um NULL statement imprime NULL se não imprime o nó
 	statementNode aux = ifstatement->statement;
 	if ( aux.children->next == NULL ){
 		//if statement is empty
 		for(i=0; i<depth+1; i++)
 			printf("  ");
 		printf("Null\n");
 	}else
 		printNode( ifstatement, depth+1 );

 	if ( ifstatement->next != NULL ){
 		//has else
	 	node* elstatement= ifstatement->next;

	 	aux = elstatement->statement;
	 	if ( aux.children->next == NULL ){
	 		//has else but statement is empty
	 		for(i=0; i<depth+1; i++)
 				printf("  ");
	 		printf("Null\n");
	 	}else
	 		printNode( elstatement, depth+1 );
 	}else{
 		//doesn't have an else
		for(i=0; i<depth+1; i++)
			printf("  ");
 		printf("Null\n");
 	}

}

void printWhileNode ( statementNode* statement, int depth ){

	node* expression = statement->children->next;
 	node* w_statement= expression->next;

 	//imprime a expression
 	printNode( expression, depth+1 );

 	int i;
 	//caso seja um NULL statement imprime NULL se não imprime o nó

 	if( w_statement == NULL || w_statement->statement.children->next == NULL ){
 		for(i=0; i<depth+1; i++)
			printf("  ");
 		printf("Null\n");
 	}else
 		printNode( w_statement, depth+1 );
}

void printFunctionNode ( functionNode* func, int depth ){
	
	int i;
	for(i=0; i<depth; i++)
		printf("  ");

	functiontype type = func->type;

	switch(type){
		case IS_FUNCDECLARATION : printf("FuncDeclaration\n"); 	break;
		case IS_FUNCDEFINITION  : printf("FuncDefinition\n"); 	break;
		case IS_FUNCDECLARATOR  : printf("FuncDeclarator\n"); 	break;
		case IS_FUNCBODY		: printf("FuncBody\n"); 		break;
		case IS_PARAMDECLARATION: printf("ParamDeclaration\n"); break;
	}

	node *aux = func->children->next;
	while(aux != NULL){
		printNode(aux, depth+1);
		aux = aux->next;
	}
}

void printDeclarationNode ( declarationNode* declaration, int depth ){
	int i;
	for(i=0; i<depth; i++)
		printf("  ");
	printf("Declaration\n");

	node *aux = declaration->children->next;
	i=0;
	while(aux != NULL){
		printNode(aux, depth+1);
		aux = aux->next;
	}
}

void printDeclaratorNode ( declaratorNode* declarator, int depth ){
	int i;
	for(i=0; i<depth; i++)
		printf("  ");

	declaratortype type = declarator->type;

	switch(type){
		case IS_SINGLE: printf("Declarator\n"); 	   break;
		case IS_ARRAY : printf("ArrayDeclarator\n"); break;
	}

	node *aux = declarator->children->next;
	while(aux != NULL){
		printNode(aux, depth+1);
		aux = aux->next;
	}

}



#ifndef SEMANTICS_H
#define SEMANTICS_H

#include "ast.h"
#include "symtab.h"

extern environment* main_env;

void checkSemantics(node *program, environment *env );
void checkProgramSemantics(programNode *program, environment *env);
void checkDeclarationSemantics(declarationNode *declaration, environment *env);
void checkDeclarator(int type_specifier, declaratorNode *declarator, environment *e);
void checkFunctionSemantics(functionNode *declaration, environment *e);
void checkStatementSemantics(statementNode *statement, environment *e);
void checkOperatorSemantics(node *n, environment *e);
void checkExpression(node *exp, int *type, int *npointer, int *is_lvalue,  environment *e);
void checkDeref(operationNode *n, int *type, int *npointer, int *is_lvalue,  environment*e);
void checkAddr(operationNode *n, int *type, int *npointer, int *is_lvalue,  environment*e);
void checkAdd(operationNode *n, int *type, int *npointer, int *is_lvalue,  environment*e);
void checkSub(operationNode *n, int *type, int *npointer, int *is_lvalue,  environment*e);
void checkMulDivMod(operationNode *n, int *type, int *npointer, int *is_lvalue,  environment*e, char op);
void checkUnary(operationNode *n, int *type, int *npointer, int *is_lvalue,  environment*e, char op);
void checkStore(operationNode *n, int *type, int *npointer, int *is_lvalue,  environment*e);
void checkCall(operationNode *n, int *type, int *npointer, int *is_lvalue, environment*e);

void getIdInfo(char *id, environment *e, int *type, int *npointer, int *is_lvalue);
void insertParam(functionNode* param, environment *e);
void compareParams(char *function_id, functionNode* param_new, symbol *param_old, int is_main );

// helper functions
sym_type termtypeToSymtype(terminaltype t);
terminaltype symtypeToTermtype(sym_type t);
int npointersParam( functionNode *param );
char * idParam(functionNode *param);
int isGlobal(environment *e);

#endif

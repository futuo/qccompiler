%{
	#include <stdio.h>
	#include <string.h>
	#include "ast.h"
	#include "symtab.h"
	#include "semantics.h"
	#include "compiler.h"

  	void initProgram();

	// int lineno = 1, colno = 1, tmp_lineno, tmp_colno;
	void yyerror (char *s);
  	extern int lineno, colno, tmp_lineno, tmp_colno;
  	extern char *yytext;
  	extern int yyleng;

  	node *prog;				// ast
  	environment *main_env; 	// main environment
%}

%token <node_t> AMP
%token <node_t> AND
%token <node_t> ASSIGN
%token <node_t> AST
%token <node_t> COMMA
%token <node_t> DIV
%token <node_t> EQ
%token <node_t> GE
%token <node_t> GT
%token <node_t> LBRACE
%token <node_t> LE
%token <node_t> LPAR
%token <node_t> LSQ
%token <node_t> LT
%token <node_t> MINUS
%token <node_t> MOD
%token <node_t> NE
%token <node_t> NOT
%token <node_t> OR
%token <node_t> PLUS
%token <node_t> RBRACE
%token <node_t> RPAR
%token <node_t> RSQ
%token <node_t> SEMI
%token <node_t> ATOI
%token <node_t> CHAR
%token <node_t> ELSE
%token <node_t> IF
%token <node_t> INT
%token <node_t> ITOA
%token <node_t> PRINTF
%token <node_t> RETURN
%token <node_t> WHILE

%token <id> ID
%token <intlit> INTLIT
%token <chrlit> CHRLIT
%token <strlit> STRLIT
%token RESERVED

%union{
	char *id;
	char *intlit;
	char *chrlit;
	char *strlit;
	struct astnode *node_t;
}

%type <node_t> start
%type <node_t> function_definition
%type <node_t> inside_function
%type <node_t> function_declaration
%type <node_t> function_declarator
%type <node_t> ast
%type <node_t> parameter_list
%type <node_t> parameter_declaration
%type <node_t> declaration
%type <node_t> type_specifier
%type <node_t> declarator
%type <node_t> declarator_list
%type <node_t> vector_declarator
%type <node_t> statement_list
%type <node_t> statement
%type <node_t> expression
%type <node_t> expression_list
%type <node_t> intlit_st
%type <node_t> declaration_list

%type <node_t> id_st
%type <node_t> chrlit_st
%type <node_t> strlit_st

// precedences
%right ASSIGN
%left OR
%left AND
%left EQ NE
%left LT GT LE GE
%left PLUS MINUS
%left DIV MOD AST
%right NOT AMP UMINUS UPLUS ADDR
%left LPAR RPAR LSQ RSQ

%nonassoc ELSE   // nonassoc ou left ??

%% 

start: 	function_definition  		{ insert(prog, $1 ); }
	| 	start function_definition	{ insert(prog, $2 ); }
	|	function_declaration 		{ insert(prog, $1 ); }
	| 	start function_declaration  { insert(prog, $2 ); }
	|	declaration 				{ insert(prog, $1 ); }
	|	start declaration 			{ insert(prog, $2 ); }
	;

function_definition: type_specifier function_declarator LBRACE inside_function RBRACE { $$ = createFunctionNode( IS_FUNCDEFINITION, $1, $2, $4, NULL ); }
				   | type_specifier function_declarator LBRACE RBRACE 				  { $$ = createFunctionNode( IS_FUNCDEFINITION, $1, $2, createFunctionNode(IS_FUNCBODY, NULL), NULL ); }
				   ;

inside_function: declaration_list statement_list { $$ = createFunctionNode(IS_FUNCBODY, $1, $2, NULL); }
			   | declaration_list 				 { $$ = createFunctionNode(IS_FUNCBODY, $1, NULL); }
			   | statement_list 				 { $$ = createFunctionNode(IS_FUNCBODY, $1, NULL); }
			   ;

declaration_list: declaration
				| declaration_list declaration  { $$ = appendNode($1, $2); }
				;

function_declaration: type_specifier function_declarator SEMI { $$ = createFunctionNode(IS_FUNCDECLARATION, $1, $2, NULL); }
					;

function_declarator: id_st LPAR RPAR					{ $$ = createFunctionNode( IS_FUNCDECLARATOR, $1, 		  NULL ); }
				   | ast id_st LPAR RPAR			 	{ $$ = createFunctionNode( IS_FUNCDECLARATOR, $1, $2, 	  NULL ); }
				   | id_st LPAR parameter_list RPAR  	{ $$ = createFunctionNode( IS_FUNCDECLARATOR, $1, $3, 	  NULL ); }
				   | ast id_st LPAR parameter_list RPAR { $$ = createFunctionNode( IS_FUNCDECLARATOR, $1, $2, $4, NULL ); }
				   ;

ast: AST 		{ $$ = createTerminalNode(IS_POINTER, NULL); }
   | ast AST 	{ $$ = appendNode( $1, createTerminalNode(IS_POINTER, NULL) ); }
   ;




parameter_list: parameter_declaration 						{ $$ = $1; }
			  | parameter_list COMMA parameter_declaration	{ $$ = appendNode( $1, $3 ); }
			  ;

parameter_declaration: type_specifier id_st 	{ $$ = createFunctionNode( IS_PARAMDECLARATION, $1, $2, 	NULL ); }
					 | type_specifier ast id_st	{ $$ = createFunctionNode( IS_PARAMDECLARATION, $1, $2, $3, NULL ); }
					 ;

declaration: type_specifier declarator_list SEMI { $$ = createDeclarationNode($1, $2); }
		   ;

type_specifier: CHAR  	{ $$ = createTerminalNode(IS_CHAR, NULL); }
			  | INT 	{ $$ = createTerminalNode(IS_INT,  NULL); }
			  ;

declarator: id_st					 	{ $$ = createDeclaratorNode( IS_SINGLE, $1, 		NULL ); }
		  | ast id_st				 	{ $$ = createDeclaratorNode( IS_SINGLE, $1, $2, 	NULL ); }
		  | id_st vector_declarator	 	{ $$ = createDeclaratorNode( IS_ARRAY , $1, $2, 	NULL ); }
		  | ast id_st vector_declarator { $$ = createDeclaratorNode( IS_ARRAY , $1, $2, $3, NULL ); }
		  ;

declarator_list: declarator
		  	   | declarator_list COMMA declarator { $$ = appendNode( $1, $3 ); }
		  	   ;

vector_declarator: LSQ intlit_st RSQ 		{ $$ = $2; }
		  		 ;

statement_list: statement
			  | statement_list statement 	{ $$ = appendNode($1, $2); }
			  ;

statement: expression SEMI 									{ $$= createStatementNode( IS_SIMPLESTAT  , $1 		  , NULL ); }
		 | SEMI 											{ $$= NULL; }
		 | LBRACE statement_list RBRACE 					{ if( $2 == NULL )
		 														$$ = NULL;
		 													  else
	   	 													    $$= createStatementNode( IS_COMPOUNDSTAT, $2, NULL ); 
		 													}
		 | LBRACE RBRACE 									{ $$= NULL; }
		 | IF LPAR expression RPAR statement 				{ $$= createStatementNode( IS_IFELSE	  , $3, $5,     NULL ); }
		 | IF LPAR expression RPAR statement ELSE statement { $$= createStatementNode( IS_IFELSE 	  , $3, $5, $7, NULL ); }
		 | WHILE LPAR expression RPAR statement 			{ $$= createStatementNode( IS_WHILE 	  , $3, $5	  , NULL ); }
		 | RETURN expression SEMI 							{ $$= createStatementNode( IS_RETURN 	  , $2 		  , NULL ); }
		 ;

expression: expression ASSIGN expression 				{ $$ = createOperationNode( IS_STORE, $1, $3, NULL ); }
		  | expression AND expression 					{ $$ = createOperationNode( IS_AND	, $1, $3, NULL ); }
		  | expression OR expression 					{ $$ = createOperationNode( IS_OR 	, $1, $3, NULL ); }
		  | expression EQ expression 					{ $$ = createOperationNode( IS_EQ 	, $1, $3, NULL ); }
		  | expression NE expression 					{ $$ = createOperationNode( IS_NE 	, $1, $3, NULL ); }
		  | expression LT expression 					{ $$ = createOperationNode( IS_LT 	, $1, $3, NULL ); }
		  | expression GT expression 					{ $$ = createOperationNode( IS_GT 	, $1, $3, NULL ); }
		  | expression LE expression 					{ $$ = createOperationNode( IS_LE 	, $1, $3, NULL ); }
		  | expression GE expression 					{ $$ = createOperationNode( IS_GE 	, $1, $3, NULL ); }
		  | expression PLUS expression 					{ $$ = createOperationNode( IS_ADD	, $1, $3, NULL ); }
		  | expression MINUS expression 				{ $$ = createOperationNode( IS_SUB 	, $1, $3, NULL ); }
		  | expression AST expression 					{ $$ = createOperationNode( IS_MUL	, $1, $3, NULL ); }
		  | expression DIV expression 					{ $$ = createOperationNode( IS_DIV 	, $1, $3, NULL ); }
		  | expression MOD expression 					{ $$ = createOperationNode( IS_MOD 	, $1, $3, NULL ); }
		  | LPAR expression RPAR						{ $$ = $2; }
		  | AMP expression                 				{ $$ = createOperationNode( IS_ADDR , $2, NULL ); }
		  | AST expression  	%prec ADDR				{ $$ = createOperationNode( IS_DEREF, $2, NULL ); }
		  | PLUS expression  	%prec UPLUS				{ $$ = createOperationNode( IS_PLUS , $2, NULL ); }
		  | MINUS expression  	%prec UMINUS			{ $$ = createOperationNode( IS_MINUS, $2, NULL ); }
		  | NOT expression  							{ $$ = createOperationNode( IS_NOT  , $2, NULL ); }
		  | expression LSQ expression RSQ				{ $$ = createOperationNode( IS_DEREF, 
		  														createOperationNode( IS_ADD, $1, $3, NULL ), NULL); }
		  | id_st LPAR RPAR								{ $$ = createOperationNode( IS_CALL , $1, NULL ); }
		  | id_st LPAR expression_list RPAR				{ $$ = createOperationNode( IS_CALL , $1, $3, NULL ); }
		  | PRINTF LPAR expression RPAR 				{ $$ = createOperationNode( IS_PRINT, $3, NULL ); }
		  | ATOI LPAR expression RPAR 					{ $$ = createOperationNode( IS_ATOI , $3, NULL ); }
		  | ITOA LPAR expression COMMA expression RPAR	{ $$ = createOperationNode( IS_ITOA , $3, $5, NULL ); }
		  | id_st
		  | chrlit_st
		  | strlit_st
		  | intlit_st
		  ;

expression_list: expression
			   | expression_list COMMA expression 		{ $$ = appendNode($1, $3); }
			   ;

id_st: 		ID 			{ $$ = createTerminalNode(IS_ID	   ,  $1); };

intlit_st: 	INTLIT		{ $$ = createTerminalNode(IS_INTLIT,  $1); };

chrlit_st: 	CHRLIT 		{ $$ = createTerminalNode(IS_CHRLIT,  $1); };

strlit_st:  STRLIT 		{ $$ = createTerminalNode(IS_STRLIT,  $1); };

%%

void yyerror (char *s) {
     printf ("Line %d, col %d: %s: %s\n", lineno, colno-(int)strlen(yytext), s, yytext);
}

void printUsage(){
	printf("Usage:\n");
	printf("\t-t\tprint the abstract syntax tree and stop after syntatic analisys.\n");
	printf("\t-s\tprint the symbol table and stop after semantic analisys.\n");
	printf("\t-c\tallways compile the program (unless errors occur)\n");
	printf("\t-o\tallways compile the program (unless errors occur) and print compiled program to file\n");
	printf("\n\tif both flags s and t are set, the proccess stops after the semantic analisys\n");
}

int main(int argc, char **argv ) {

	int printAst = 0,
		printSym = 0,
		_compile = 0,
		i;

	FILE *dest = fopen("result.c", "w");

	for( i=1; i<argc; i++ ){
		
		if( strcmp(argv[i], "-st") == 0 || strcmp(argv[i], "-ts") == 0 )
			printAst = printSym = 1;
		else if( strcmp(argv[i], "-t") == 0 )
			printAst = 1;
		else if( strcmp(argv[i], "-s") == 0 )
			printSym = 1;
		else if( strcmp(argv[i], "-c") == 0 )
			_compile = 1;
		else if( strcmp(argv[i], "-o") == 0 ){
			_compile = 1;
			dest = fopen( argv[i+1], "w" );
			i += 1;
		}

	}

	initProgram();

	if( !yyparse() ){

		if(printAst){
			printNode(prog, 0);
			if(!printSym)
				return 0;
		}

		checkSemantics(prog, main_env);

		if(printSym){
			printSymtab(main_env);
			if(!_compile)
				return 0;
		}
		dest = fopen("result.c", "w");
		compile(dest, prog );
		fclose(dest);
	}
	return 0;
}

void initProgram(){
	// AST
	prog = (node *) malloc(sizeof(node));
  	prog->type = PROGRAM;
  	prog->next = NULL;
  	prog->program.children = (node *)malloc(sizeof(node));
  	prog->program.children->next = NULL;

  	// symtabs
  	main_env = newEnvironment(NULL);

}


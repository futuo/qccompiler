#!/usr/bin/python2
# -*- coding: utf-8 -*-

# Requirements: Python 2.6 or Python 2.7
# Mooshak command-line uploading tool
# To use this script:
# '$ ./mooshak-cmd' for just showing results
# '$ ./mooshak-cmd program.c' to upload a file and show the results

from __future__ import print_function

#-------------------------------------
# ------- OPTIONS AND CONFIG ---------

URL = 'http://mooshak.dei.uc.pt/~comp2013'
CONTEST = 'Meta1'
PROBLEM_LIST = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'U', 'J', 'K', 'L', 'M']

USER = 'lex'
PASS = 'LUAFrQ'

LINES = 30

# ---------- END OF CONFIG -----------
# ------------------------------------

import MultipartPostHandler
import urllib2, cookielib
import re, time, sys
from urllib import urlencode
from HTMLParser import HTMLParser


problems = dict()
sendfile = None
results = list()
subresult = None

if len(sys.argv) > 1:
    sendfile = open(sys.argv[1], "rb")

class ProblemParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        if tag == 'input':
            dattrs = dict(attrs)
            if dattrs.get('name') == 'problem' and dattrs.get('value'):
                problems[dattrs.get('value')] = dattrs.get('title').decode('ISO 8859-1')

class ProblemCompilersParser(HTMLParser):
    startParse = False
    probs = []
    def handle_starttag(self, tag, attrs):
        if tag == 'select' and ('name', 'problem') in attrs:
            self.startParse = True
            self.probs = []
        if tag == 'option' and self.startParse:
            dattrs = dict(attrs)
            problems[dattrs.get('value')] = dattrs.get('value')
            self.probs.append(dattrs.get('value'))
    def handle_data(self, data):
        if self.startParse:
            #pass        #pode ser usado para pescar o titulo do prob
            problems[self.probs[-1]] = data
    def handle_endtag(self, tag):
        if tag == 'select':
            self.startParse = False

class ResultsParser(HTMLParser):
    saveddata = ""
    def handle_starttag(self, tag, attrs):
        if tag == 'tr':
            results.append(list())
    def handle_endtag(self, tag):
        if tag == 'td':
            results[-1].append(self.saveddata.strip())
            self.saveddata = ""
    def handle_data(self, data):
        self.saveddata += data

class SubmissionParser(HTMLParser):
    saveddata = ""
    subresult = None
    def handle_endtag(self, tag):
        if tag == 'h3':
            self.subresult = self.saveddata
    def handle_data(self, data):
        self.saveddata = data


cookies = cookielib.CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookies))

# Página inicial
response = opener.open(URL+'/cgi-bin/execute')
htmlpage = response.read()
mooshak_session = re.search(r'action="([^?]+)?', htmlpage).group(1)

# Página de login
response = opener.open(URL+'/cgi-bin/execute/' + mooshak_session + '?login')
htmlpage = response.read()

# Autenticação

values = urlencode({ 'command': 'login', 'arguments': '',
                   'contest': CONTEST,
                   'user': USER, 'password': PASS })
response = opener.open( URL + '/cgi-bin/execute/' + mooshak_session, values )
htmlpage = response.read()

# Encontra os problemas disponíveis

response = opener.open( URL+'/cgi-bin/execute/' + mooshak_session + '?htools' )
htmlpage = response.read()

##parser = ProblemParser()
parser = ProblemCompilersParser()
parser.feed(htmlpage)

PROBLEM_LIST = [p for p in PROBLEM_LIST if p in problems]

if not PROBLEM_LIST:
    print("Current problems available:")
    for problem,name in sorted(problems.items(), key=lambda x:x[1]):
        print(name, problem)
    exit()

# Envia o ficheiro
if sendfile:
    for prob in PROBLEM_LIST:
        #time.sleep(2)
        print("A submeter...", problems[prob], end=' ')
        sys.stdout.flush()
        openermp = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookies),
                                        MultipartPostHandler.MultipartPostHandler)
        params = { "command" : "analyze", "problem" : prob,
                   "analyze" : "Submit", "program" : sendfile }
        response = openermp.open(URL+'/cgi-bin/execute/' + mooshak_session + "?", params)
        htmlpage = response.read()
        
        parser = SubmissionParser()
        parser.feed(htmlpage)
        subresult = parser.subresult
        print(':', subresult)


# Intro do programa
    
print(' Mooshak upload tool '.center(50, '-'))
print('  ' + URL)
print('  Username:', USER)
print('  Contest:', CONTEST)
print('  Problems:', *PROBLEM_LIST, sep=' ')
if sendfile:
    print('  Submited file:', sendfile.name)
    print('  Status:', subresult)
else:
    print('  No file to submit')
print("".ljust(50,'-'))
print()

# Mostra Resultados

#tipos de tempo: absolute, relative, contest
#all_teams=on para toda a gente
#teams=testtest para apenas 1
values = urlencode({ 'all_problems': 'on', 'page': '0', 'teams': USER,
                     'type': 'submissions', 'lines': LINES, 'time': '5',
                     'command': 'listing', 'time_type': 'absolute' })

response = opener.open( URL+'/cgi-bin/execute/' + mooshak_session + '?' + values )
htmlpage = response.read()

parser = ResultsParser()
parser.feed(htmlpage)


#print(htmlpage)
#print(results)

print("Results:");
fieldsname = ['Num', 'Time', 'Country', 'Team', 'Problem', 'Language', 'Result', 'CPU', 'State']
#fields = [(0,3),(1,20),(3,20),(4,7),(5,12), (6,25), (7,10)]

#compiladores
fields = [(0,4),(1,20),(3,20),(4,7),(5,13), (6,25)]

print(*("".ljust(l, '=') for (f,l) in fields), sep = '===');
print(*(fieldsname[f].center(l) for (f,l) in fields), sep = ' | ');
print(*("".ljust(l, '-') for (f,l) in fields), sep = '-+-');
for line in results:
    if len(line) >= 8:
        print(*(line[f].rjust(l) for (f,l) in fields), sep = ' | ');
print(*("".ljust(l, '-') for (f,l) in fields), sep = '---');

# Log out
opener.open(URL+'/cgi-bin/execute/' + mooshak_session + "?logout").read()

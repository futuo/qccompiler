#ifndef COMPILER_H
#define COMPILER_H

#include <stdio.h>
#include <stdlib.h>
#include "symtab.h"
#include "ast.h"
#include "semantics.h"

int compile(FILE *dest, node *ast);
void printHeader(FILE *dest);
void printFooter(FILE *dest);
void printProgram(FILE *dest, node *ast, environment *pe);
void printVariableDeclaration(FILE *dest, node *ast, environment *e);
void printRedirector(FILE* dest);
void printDeclarations(FILE *dest, node *ast, environment *e);
int printArithmeticOperation(FILE *dest, node *ast, environment *e, char op);
void printStoreOperation(FILE *dest, node *ast, environment *e);
void printMain( FILE *dest, node *ast, environment *e );
char* getSymId( char *id, environment *e, int *type, int *npointer );
int printUnaryOperation(FILE *dest, node *ast, environment *e, char op);

void printGlobalDeclaration(FILE *dest, symbol *s);
void printDeclaration(FILE *dest, symbol *s, char *saveto);


int printExpression(FILE *dest, node *exp, environment*e, int *type, int *npointer, int *is_lvalue, int give_me_ref);
int loadID(FILE *dest, node *ast, environment *e, int *type, int *npointer, int *is_lvalue, int give_me_ref);
int printBinary(FILE *dest, node *ast, environment *e, char *op, int *type, int *npointer, int *is_lvalue, int give_me_ref);
int printOperation(FILE *dest, node *ast, environment*e);
int loadLit(FILE *dest, node *ast, environment *e, int *type, int *npointer, int *is_lvalue, int give_me_ref);
int printItoaOperation(FILE *dest, node *ast, environment *e, int *type, int *npointer, int *is_lvalue, int give_me_ref);
void printPrintf(FILE *dest, node *ast, environment *e, int *type, int *npointer, int *is_lvalue, int give_me_ref);
int printAtoiOperation(FILE *dest, node *ast, environment *e, int *type, int *npointer, int *is_lvalue, int give_me_ref);
int printAddSub(FILE *dest, node *ast, environment *e, int *type, int *npointer , char op, int *is_lvalue, int give_me_ref);
int printUnary(FILE *dest, node *ast, environment *e, int *type, int *npointer, char op, int *is_lvalue, int give_me_ref);
int printNot(FILE *dest, node *ast, environment *e, int *type, int *npointer, int *is_lvalue, int give_me_ref);
int printDeref(FILE *dest, node *ast, environment *e, int *type, int *npointer, int *is_lvalue, int give_me_ref);
int printStore(FILE *dest, node *ast, environment *e, int *type, int *npointer, int *is_lvalue, int give_me_ref);
int printAddr(FILE *dest, node *ast, environment *e, int *type, int *npointer, int *is_lvalue, int give_me_ref);
int printFunctionCall(FILE *dest, node *ast, environment *e, int *type_p, int *npointer_p);
void printFunction( FILE *dest, node *ast, environment *e );
int printAnd(FILE *dest, node *ast, environment *e, char *op, int *type, int *npointer, int *is_lvalue, int give_me_ref);
int printOr(FILE *dest, node *ast, environment *e, char *op, int *type, int *npointer, int *is_lvalue, int give_me_ref);

void printStatement(FILE *dest, node *ast, environment *e);
void printIfElse( FILE *dest, node* ifElseStat, environment *e );
void printWhile( FILE *dest, node* whileStat, environment *e );
void printReturn( FILE *dest, node *ast, environment *e );

#endif

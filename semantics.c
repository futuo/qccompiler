#include "semantics.h"
#include "symtab.h"
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>

#define max(a, b) ((a) > (b) ? (a) : (b))

void checkSemantics(node *n, environment *e){
	
	if(n){
		nodetype type = n->type;
		switch(type){
			case PROGRAM 	: checkProgramSemantics 	( &(n->program)    , e );	break;
			case DECLARATION: checkDeclarationSemantics ( &(n->declaration), e );	break;
			case FUNCTION 	: checkFunctionSemantics	( &(n->function)   , e );	break;
			case STATEMENT  : checkStatementSemantics   ( &(n->statement)  , e );   break;
			case OPERATOR 	: // operator same as terminal
			case TERMINAL 	: checkOperatorSemantics 	( n  , e );   				break;
			default :    break;
		}
	}
}

void checkProgramSemantics(programNode *program, environment *e){
	node *it;

	for(it = program->children->next; it; it=it->next){
		checkSemantics(it, e);
	}

	// check for main existence and if functions are used and declared but not defined
	int found_main = 0;
	environment *it2 = e->envs->next;
	for( ; it2 ; it2 = it2->next){
		if( strcmp(it2->id, "main")==0 ){
			found_main = 1;
			if( !it2->is_defined ){
				printf("Function main is not defined\n");
				exit(0);
			}
		}
		else if(!it2->is_defined && it2->used){
			printf("Function %s is declared and used but is not defined\n", it2->id);
			exit(0);
		}
	}
	if(!found_main){
		printf("Function main is not defined\n");
		exit(0);
	}
}

void checkDeclarationSemantics(declarationNode *declaration, environment *e){
	node *it;
	int type_specifier = declaration->children->next->terminal.type;

	for(it = declaration->children->next->next; it ; it = it->next){
		checkDeclarator(type_specifier, &(it->declarator), e);
	}
}

void checkDeclarator(int type_specifier, declaratorNode *declarator, environment *e){
	node *it;
	int npointer = 0, narray = 0;
	char id[128];
	sym_type type = termtypeToSymtype(type_specifier);

	// count number of pointers
	for(it = declarator->children->next; it->terminal.type==IS_POINTER ; it=it->next){
		npointer++;
	}
	if(it){
		strcpy(id, it->terminal.value);
		if(it->next){  // is an ArrayDeclarator
			narray = atoi(it->next->terminal.value);
			if(narray <= 0){
				printf("Size of array %s is not positive\n", id);
				exit(0);
			}
		}

		if(isGlobal(e)){
			if(!insertGlobalSym(e, type, id, npointer, narray)){
				printf("Symbol %s redefined\n", id);
				exit(0);				
			}
		}
		else{
			if( !insertSym(e, type, id, npointer, narray, 0, 0) ){
				printf("Symbol %s redefined\n", id);
				exit(0);
			}
		}
	}
	else{
		printf("ERRO ESTUPIDO\n");
	}
}

int isGlobal(environment *e){
	return !e->id;
}

void insertMain(environment *e){
	insertFuncSym(e, "main");
	environment *me = insertEnvironment(e, "main");
	if(me){
		insertSym(me, _int, "", 0, 0, 0, 1);	// insert return
		insertSym(me, _int, " ", 0, 0, 1, 0);	// insert int
		insertSym(me, _char, "  ", 2, 0, 1, 0);	// insert char **
		me->nparams = 2;
	}
	else{
		printf("Não devia dar o erro (a inserir environment do main)\n");
		exit(0);
	}	
}

void checkFunctionSemantics(functionNode *function, environment *e){
	node *it, *it2;
	sym_type type;
	char id[128];
	environment *fe;  // function new environment
	int is_funcdef = function->type == IS_FUNCDEFINITION;
	int npointer;
	int tmp;
	int is_main = 0;

	// first child - function return type
	it = function->children->next;
	type = termtypeToSymtype(it->terminal.type);

	it = it->next;
	// second child - function declarator
	it2 = it->function.children->next;  // iterator for FuncDeclarator children

	// count number of pointers
	npointer = 0;
	for( ; it2->terminal.type == IS_POINTER; it2 = it2->next, npointer++ );
	// store function id
	strcpy(id, it2->terminal.value);

	// if its 'main'
	if(strcmp(id, "main")==0){
		// insert default arguments and return type
		insertMain(e);
		is_main = 1;
	}
	
	if( (tmp=insertFuncSym(e, id))==1 ){		// function does not exist yet as symbol or environment
		fe = insertEnvironment(e, id);
		if(fe){
			if(is_funcdef)
				fe->is_defined = 1;
			// insert return symbol
			if(!insertSym(fe, type, "", npointer, 0, 0, 1)){
				printf("Erro a inserir return symbol no env %s\n", fe->id);
				exit(0);
			}
			// insert parameters
			int nparams = 0;
			for( it2 = it2->next; it2; it2 = it2->next, nparams++){
				insertParam( &(it2->function), fe );
			}
			fe->nparams = nparams;
		}
		else{
			printf("Erro a criar environment %s\n", id);
			exit(0);
		}
	}
	else if(!tmp){
		printf("Symbol %s redefined\n", id);
		exit(0);
	}
	else{   									// function already exists
		fe = searchEnvironment(e, id);
		if(fe){
			if(fe->is_defined && is_funcdef){
				printf("Function %s redefined\n", id);
				exit(0);				
			}
			else {
				if(is_funcdef)
					fe->is_defined = 1;

				// check if function type is the same
				symbol *r = fe->symtab->next;  // return is always the first child
				if( type == r->type ){
					if( npointer != r->npointer ){
						printf("Conflicting types in function %s declaration/definition (got %s, required %s)\n", 
								id, symtypeToStr(type, npointer), symtypeToStr(r->type, r->npointer));
						exit(0);
					}
				}
				else {
					printf("Conflicting types in function %s declaration/definition (got %s, required %s)\n", 
							id, symtypeToStr(type, npointer), symtypeToStr(r->type, r->npointer));
					exit(0);
				}

				// check parameters
				symbol *it3 = fe->symtab->next->next;   // first parameter of already defined function
				it2 = it2->next; 						// first parameter of FuncDeclarator of current function
				int nparams = 0;
				for( ; it2 && it3 && it3->is_param; it2 = it2->next, it3 = it3->next, nparams++){
					compareParams( id, &(it2->function), it3, is_main );
				}
				if(!it2){
					if(it3 && it3->is_param){
						printf("Conflicting numbers of arguments in function %s declaration/definition (got %d, required %d)\n", id, nparams, fe->nparams);
						exit(0);						
					}
				}
				else if(!it3 || (it3 && !it3->is_param)){
					if(it2){
						// count number of remainder parameters
						for(;it2; it2=it2->next, nparams++);
						printf("Conflicting numbers of arguments in function %s declaration/definition (got %d, required %d)\n", id, nparams, fe->nparams);
						exit(0);						
					}
				}
			}
		}
		else{
			printf("Erro estupido funcao %s devia existir como environment\n", id);
			exit(0);
		}
	}

	// checkSemantics for all instruction inside body
	if(is_funcdef){
		it = it->next;
		// third child - function body
		it = it->function.children->next;
		for( ; it; it = it->next ){
			checkSemantics( it , fe);
		}
	}
}

void checkStatementSemantics(statementNode *statement, environment *e){
	node *n;
	switch(statement->type){
		case IS_SIMPLESTAT :
			checkSemantics(statement->children->next, e);		
		break;
		case IS_IFELSE:
			// always has 3 children
			checkSemantics(statement->children->next, 			  	e);		// condition
			checkSemantics(statement->children->next->next, 		e);		// if
			checkSemantics(statement->children->next->next->next, 	e);		// else
		break;
		case IS_COMPOUNDSTAT:
			n = statement->children->next;
			for(; n; n=n->next){
				checkSemantics(n, e);
			}
		break;
		case IS_WHILE:
			checkSemantics(statement->children->next, 		e);		// condition
			checkSemantics(statement->children->next->next, e);		// body
		break;
		case IS_RETURN :
			checkSemantics(statement->children->next, e);
		break;
	}
}

void checkOperatorSemantics(node *n, environment *e){
	// node * instead of operationNode * because expressions can be terminal or operators
	int type, npointer, is_lvalue;
	checkExpression(n, &type, &npointer, &is_lvalue, e);
}

void checkExpression(node *exp, int *type, int *npointer, int *is_lvalue, environment *e){
	// returns by out parameters 'type', 'npointers' and 'is_lvalue' 
	// 'npointers' decrements with '*' and increments with '&'

	// condição de paragem
	if(exp->type == TERMINAL){
		terminaltype t = exp->terminal.type;
		switch(t){
			case IS_INTLIT 	: case IS_CHRLIT :
				*type = t;
				*npointer = 0;
				*is_lvalue = 0;
			break;
			case IS_STRLIT 	:
				*type = IS_CHRLIT;
				*npointer = 1;
				*is_lvalue = 0;
			break;
			case IS_ID 	   	:
				getIdInfo(exp->terminal.value, e, type, npointer, is_lvalue);
			break;
			default : break;
		}
	}
	else if(exp->type == OPERATOR){
		switch( exp->operation.type ){
			case IS_DEREF:
				checkDeref( &(exp->operation), type, npointer, is_lvalue, e );
			break;
			case IS_ADDR:
				checkAddr( &(exp->operation), type, npointer, is_lvalue, e );
			break;
			case IS_ADD:
				checkAdd( &(exp->operation), type, npointer, is_lvalue, e );
			break;
			case IS_SUB:
				checkSub( &(exp->operation), type, npointer, is_lvalue, e );
			break;
			case IS_MUL:
				checkMulDivMod( &(exp->operation), type, npointer, is_lvalue, e, '*' );
			break;
			case IS_DIV:
				checkMulDivMod( &(exp->operation), type, npointer, is_lvalue, e, '/' );
			break;
			case IS_MOD:
				checkMulDivMod( &(exp->operation), type, npointer, is_lvalue, e, '%' );
			break;
			case IS_MINUS:
				checkUnary( &(exp->operation), type, npointer, is_lvalue, e, '-' );
			break;
			case IS_PLUS :
				checkUnary( &(exp->operation), type, npointer, is_lvalue, e, '+' );
			break;
			case IS_NOT :
				checkExpression(exp->operation.children->next, type, npointer, is_lvalue, e);
				*type = IS_INTLIT;
				*npointer = 0;
				*is_lvalue = 0;
			break;
			case IS_STORE:
				checkStore( &(exp->operation), type, npointer, is_lvalue, e );
			break;
			case IS_CALL:
				checkCall( &(exp->operation), type, npointer, is_lvalue, e );
			break;
			case IS_PRINT: case IS_ATOI :
				checkExpression(exp->operation.children->next, type, npointer, is_lvalue, e );
				*type = IS_INTLIT;
				*npointer = 0;
				*is_lvalue = 0;
			break;
			case IS_ITOA :
				checkExpression(exp->operation.children->next, 		 type, npointer, is_lvalue, e );
				checkExpression(exp->operation.children->next->next, type, npointer, is_lvalue, e );
				*type = IS_CHRLIT;
				*npointer = 1;
				*is_lvalue = 0;
			break;
			default: //  IS_OR, IS_AND, IS_EQ, IS_NE, IS_LT, IS_GT, IS_LE, IS_GE
				checkExpression(exp->operation.children->next, 		 type, npointer, is_lvalue, e );
				checkExpression(exp->operation.children->next->next, type, npointer, is_lvalue, e );
				*type = IS_INTLIT;
				*npointer = 0;
				*is_lvalue = 0;			
			break;
		}
	}
	else{
		printf("This should not happen expression must be composed of terminal and operations nodes only\n");
		exit(0);
	}
}

void checkCall(operationNode *n, int *type, int *npointer, int *is_lvalue, environment*e){
	char *call_id = n->children->next->terminal.value;
	if(!searchSym(call_id, e)){
		environment *fe = searchEnvironment(main_env, call_id);
		if(fe){
			fe->used = 1;
			// check arguments
			node *it = n->children->next->next;   // iterator for parameters
			int cnt = 0;
			for(; it; it = it->next, cnt++){
				// validate each argument in case it is an operator (expression)
				checkSemantics(it, e);
			}
			if(cnt != fe->nparams){
				printf("Wrong number of arguments in call to function %s (got %d, required %d)\n", call_id, cnt, fe->nparams);
				exit(0);
			}

			if(fe->symtab->next->is_return){
				*type = symtypeToTermtype( fe->symtab->next->type );
				*npointer = fe->symtab->next->npointer;
				*is_lvalue = 0;
			}
			else{
				printf("First child of function %s is not a return child\n", call_id);
				exit(0);
			}
		}
		else{
			if( !searchSym(call_id, main_env) ){
				printf("Unknown symbol %s\n", call_id);
				exit(0);
			}
			else{
				printf("Symbol %s is not a function\n", call_id);
				exit(0);
			}
		}
	}
	else{
		printf("Symbol %s is not a function\n", call_id);
		exit(0);
	}
}

void checkDeref(operationNode *n, int *type, int *npointer, int *is_lvalue, environment*e){
	checkExpression(n->children->next, type, npointer, is_lvalue, e);
	if((*npointer)>0){
		(*npointer)--;
		*is_lvalue = 1;
	}
	else{
		printf("Operator * cannot be applied to type %s\n", termtypeToStr(*type, *npointer));
		exit(0);
	}
}

void checkAddr(operationNode *n, int *type, int *npointer, int *is_lvalue,  environment*e){
	checkExpression(n->children->next, type, npointer, is_lvalue, e);
	if(*is_lvalue){
		(*npointer)++;
		*is_lvalue = 0;
	}
	else{
		printf("Lvalue required\n");
		exit(0);
	}
}

void checkAdd(operationNode *n, int *type, int *npointer, int *is_lvalue,  environment*e){
	int type1, npointer1, is_lvalue1;
	int type2, npointer2, is_lvalue2;

	checkExpression(n->children->next, 		 &type1, &npointer1, &is_lvalue1, e);
	checkExpression(n->children->next->next, &type2, &npointer2, &is_lvalue2, e);

	if( npointer1 > 0  && npointer2 >0 ){
		printf("Operator + cannot be applied to types %s, %s\n", termtypeToStr(type1,npointer1), termtypeToStr(type2,npointer2));
		exit(0);
	}
	*type = IS_INTLIT;
	*npointer = max(npointer1, npointer2);
	*is_lvalue = 0;
}

void checkSub(operationNode *n, int *type, int *npointer, int *is_lvalue,  environment*e){
	int type1, npointer1, is_lvalue1;
	int type2, npointer2, is_lvalue2;

	checkExpression(n->children->next, 		 &type1, &npointer1, &is_lvalue1, e);
	checkExpression(n->children->next->next, &type2, &npointer2, &is_lvalue2, e);

	if( npointer1 != npointer2 ){
		if( npointer1 == 0 || npointer2 > 0 ){
			printf("Operator - cannot be applied to types %s, %s\n", termtypeToStr(type1,npointer1), termtypeToStr(type2,npointer2));
			exit(0);
		}
	}
	else if(type1 != type2){
		printf("Operator - cannot be applied to types %s, %s\n", termtypeToStr(type1,npointer1), termtypeToStr(type2,npointer2));
		exit(0);		
	}
	*type = IS_INTLIT;
	*npointer = abs(npointer1-npointer2);
	*is_lvalue = 0;
}

void checkMulDivMod(operationNode *n, int *type, int *npointer, int *is_lvalue,  environment*e, char op){
	int type1, npointer1, is_lvalue1;
	int type2, npointer2, is_lvalue2;

	checkExpression(n->children->next, 		 &type1, &npointer1, &is_lvalue1, e);
	checkExpression(n->children->next->next, &type2, &npointer2, &is_lvalue2, e);

	if(!(npointer1==0 && npointer2 == 0)){
		printf("Operator %c cannot be applied to types %s, %s\n", op, termtypeToStr(type1,npointer1), termtypeToStr(type2,npointer2));
		exit(0);		
	}
	*type = IS_INTLIT;
	*npointer = 0;
	*is_lvalue = 0;
}

void checkUnary(operationNode *n, int *type, int *npointer, int *is_lvalue,  environment*e, char op){
	// MINUS, PLUS
	checkExpression(n->children->next, type, npointer, is_lvalue, e);
	if((*npointer)>0){
		printf("Operator %c cannot be applied to type %s\n", op, termtypeToStr(*type, *npointer));
		exit(0);				
	}
	*is_lvalue = 0;
}

void checkStore(operationNode *n, int *type, int *npointer, int *is_lvalue,  environment*e){
	int type1, npointer1, is_lvalue1;
	int type2, npointer2, is_lvalue2;

	checkExpression(n->children->next, 		 &type1, &npointer1, &is_lvalue1, e);
	if(!is_lvalue1){
		printf("Lvalue required\n");
		exit(0);
	}
	checkExpression(n->children->next->next, &type2, &npointer2, &is_lvalue2, e);

	*type = type1;
	*npointer = npointer1;
	*is_lvalue = 1;
}

void getIdInfo(char *id, environment *e, int *type, int *npointer, int *is_lvalue){
	// search id in environment 'e' and global environment
	// returns by out parameters 'type' and 'npointers'

	symbol *s = searchSym(id, e);
	if(!s){
		if( !(s=searchSym(id, main_env) ) ){
			printf("Unknown symbol %s\n", id);
			exit(0);
		}
	}
	if(s->type != _function){
		*type = symtypeToTermtype(s->type);
		*npointer = s->npointer + (s->narray > 0) ;
		*is_lvalue = 1;
	}
	else{     // if is function check return type
		environment *fe = searchEnvironment(main_env, id);
		if(fe){
			if(fe->symtab->next->is_return){
				*type = symtypeToTermtype( fe->symtab->next->type );
				*npointer = fe->symtab->next->npointer;
				*is_lvalue = 0;
			}
			else{
				printf("First child of function %s is not a return child\n", id);
				exit(0);
			}			
		}
		else{
			printf("This should not happen! Function %s must exist as environment\n", id);
			exit(0);
		}
	}
}

void insertParam(functionNode* param, environment *e){
	// similar to checkDeclarator
	node *it;
	int npointer = 0;
	char id[128];

	// first child - parameter type
	sym_type type = termtypeToSymtype(param->children->next->terminal.type);

	// count number of pointers
	it = param->children->next->next;
	for( ; it->terminal.type==IS_POINTER ; it=it->next){
		npointer++;
	}
	if(it){
		strcpy(id, it->terminal.value);

		if( !insertSym(e, type, id, npointer, 0, 1, 0) ){
			printf("Symbol %s redefined\n", id);
			exit(0);
		}
	}
	else{
		printf("ERRO ESTUPIDO (declaraco de parametro sem id)\n");
	}
}

void compareParams(char *function_id, functionNode* param_new, symbol *param_old, int is_main ){
	// check against types first
	int npointer_new = npointersParam(param_new);
	sym_type type_new = termtypeToSymtype(param_new->children->next->terminal.type);

	int npointer_old = param_old->npointer;
	sym_type type_old = param_old->type;

	if( type_new == type_old ){
		if(npointer_new != npointer_old){
			printf("Conflicting types in function %s declaration/definition (got %s, required %s)\n",
					function_id, symtypeToStr(type_new,npointer_new), symtypeToStr(type_old, npointer_old));
			exit(0);
		}
	}
	else{
		printf("Conflicting types in function %s declaration/definition (got %s, required %s)\n",
				function_id, symtypeToStr(type_new,npointer_new), symtypeToStr(type_old, npointer_old));
		exit(0);
	}

	// check against argument names
	char *id_new = idParam(param_new);
	char *id_old = param_old->id;
	if( strcmp(id_new, id_old) ){
		if(is_main){
			// replace default argument names for 'main': int' ' and char **'  '
			if( strcmp(id_old, " ")==0 || strcmp(id_old, "  ")==0 ){
				strcpy(param_old->id, id_new);
			}
			else{
				printf("Conflicting argument names in function %s declaration/definition (got %s, required %s)\n",
						function_id, id_new, id_old);
				exit(0);				
			}
		}
		else{
			printf("Conflicting argument names in function %s declaration/definition (got %s, required %s)\n",
					function_id, id_new, id_old);
			exit(0);					
		}
	}
}

sym_type termtypeToSymtype(terminaltype t){
	// converts terminal type to sym_type: IS_INT -> _int, IS_CHAR -> _char
	switch(t){
		case IS_INT:  return _int;  break;
		case IS_CHAR: return _char; break;              
		default: break;
	}
	printf("This cannot happen (trying to translate termtype)\n" );
	return -1;
}

terminaltype symtypeToTermtype(sym_type t){
	// converts sym_type to terminal type _int -> IS_INT, _char -> IS_CHAR
	switch(t){
		case _int:  return IS_INTLIT;  break;
		case _char: return IS_CHRLIT; break;              
		default: break;
	}
	printf("This cannot happen (trying to translate sym_type)\n" );
	return -1;
}

int npointersParam( functionNode *param ){
	// count number of pointers of param
	int npointer = 0;
	node *it = param->children->next->next;   // skip param id
	for( ; it->terminal.type==IS_POINTER ; it=it->next){
		npointer++;
	}	
	return npointer;
}

char * idParam(functionNode *param){
	// returns ID of ParamDeclaration (last child on linked list)
	node *n = param->children->next;
	while(n->next) n = n->next;

	if(n->terminal.type == IS_ID)
		return n->terminal.value;
	else{
		printf("This cannot happen (last child on ParamDeclaration children linked list is not ID)!\n");
		exit(0);
	}
}

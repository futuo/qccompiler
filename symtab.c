#include <stdio.h>
#include <string.h>
#include "symtab.h"


int insertSym(environment *e, sym_type t, char *id, int npointer, int narray, char is_param, char is_return){
	symbol *ptr;

	// advance to last position of symtab linked list
	for( ptr = e->symtab; ptr->next; ptr=ptr->next )
		if( strcmp(ptr->next->id, id)==0 )
			return 0;

	ptr->next = newSymbol(id, t, npointer, narray, is_param, is_return);
	
	if( !is_return && t != _function )
		ptr->next->offset = e->nvars++;
	else
		ptr->next->offset = -1;

	return 1;
}

int insertFuncSym(environment *e, char *id){
	// returns: 1-successfully inserted,  0-symbol already exists,   -1-symbol already exists as function
	symbol *ptr;

	// advance to last position of symtab linked list
	for( ptr = e->symtab; ptr->next; ptr=ptr->next )
		if( strcmp(ptr->next->id, id)==0 ){
			return ptr->next->type == _function ? -1 : 0;
		}

	ptr->next = newSymbol(id, _function, 0, 0, 0, 0);
	ptr->next->offset = -1;

	return 1;
}

int insertGlobalSym(environment *e, sym_type t, char *id, int npointer, int narray){
	symbol *ptr;

	// advance to last position of symtab linked list
	for( ptr = e->symtab; ptr->next; ptr=ptr->next )
		if( strcmp(ptr->next->id, id)==0 ){
			if( ptr->next->type != t) return 0;
			else return 1;
		}

	ptr->next = newSymbol(id, t, npointer, narray, 0, 0);
	if( t != _function )
		ptr->next->offset = e->nvars++;
	else
		ptr->next->offset = -1;

	return 1;
}

symbol *searchSym(char *id, environment *e){
	symbol *it;

	for(it = e->symtab->next; it; it = it->next){
		if( strcmp(it->id, id)==0 )
			return it;
	}
	return NULL;
}

environment* newEnvironment(char *id){
	environment* e = (environment *) malloc(sizeof(environment)) ;
	if(e){
		if(id){
			e->id = (char *) malloc((strlen(id)+1)*sizeof(char));
			strcpy(e->id, id);
		}
		else
			e->id = NULL;  // global environment

		e->symtab     = (symbol *) malloc(sizeof(symbol));
		e->envs       = (environment *) malloc(sizeof(environment));
		e->next       = NULL;
		e->nparams    = 0;
		e->is_defined = 0;
		e->used       = 0;
		e->nvars	  = 0;
	}
	else
		printf("Erro a criar environment em newEnvironment(%s)\n", id);

	return e;

}

environment* searchEnvironment(environment* parent, char *env_id){
	// returns pointer to the environment (inside 'parent'), NULL if not found
	environment *it = parent->envs->next;
	for( ; it; it = it->next)
		if( strcmp(it->id, env_id)==0 )
			return it;

	return NULL;
}

environment* insertEnvironment(environment* parent, char *env_id){
	// returns pointer to environment inserted
	environment *it = parent->envs;
	for( ; it->next; it = it->next);
	it->next = newEnvironment(env_id);

	return it->next;
}

symbol* newSymbol(char *id, sym_type t, int npointer, int narray, char is_param, char is_return){
	
	symbol *s = (symbol *) malloc(sizeof(symbol));
	if(s){
		if(id){
			s->id        = (char *) malloc((strlen(id)+1)*sizeof(char));
			strcpy(s->id , id);
		}
		else
			s->id = NULL;  // for returns
		s->type      = t;
		s->npointer  = npointer;
		s->narray    = narray;
		s->is_param  = is_param;
		s->is_return = is_return;
		s->next      = NULL;
	}
	return s;
}

void printSymtab(environment *e){

	if(!e)
		return;

	if( e->id == NULL )
		printf("===== Global Symbol Table =====\n");
	else
		printf("===== Function %s Symbol Table =====\n", e->id);

	int i;
	symbol* it = e->symtab->next;
	for( ; it; it = it->next ){

		if(!it->is_return)
			printf("%s", it->id);
		else
			printf("return");

		switch(it->type){
			case _int:
				printf("\tint");
				break;
			case _char:
				printf("\tchar");
				break;
			case _function:
				printf("\tfunction");
				break;				
		}

		for(i=0; i<it->npointer; i++)
			printf("*");

		if( it->narray )
			printf("[%d]", it->narray);

		if( it->is_param )
			printf("\tparam");

		printf("\n");
	}

	environment *ite;
	ite = e->envs->next;
	for( ; ite; ite = ite->next)
		printSymtab(ite);

}

char *symtypeToStr(sym_type t, int npointers){
	int i;
	char *str = (char *)malloc(50*sizeof(char));
	if(str){
		switch(t){
			case _int: strcpy(str, "int"); 	 break;
			case _char: strcpy(str, "char"); break;
			default: break;
		}
		for( i = 0; i < npointers; ++i )
			strcat(str, "*");
		
		return str;
	}
	else{
		printf("Erro a alocar memoria para str em symtypeToStr\n");
		exit(EXIT_FAILURE);
	}
	return NULL;
}

char *termtypeToStr(int t, int npointers){
	int i;
	char *str = (char *)malloc(50*sizeof(char));
	if(str){
		switch(t){
			case IS_INTLIT: strcpy(str, "int");  break;
			case IS_CHRLIT: strcpy(str, "char"); break;
			default: break;
		}
		for( i = 0; i < npointers; ++i )
			strcat(str, "*");

		return str;
	}
	else{
		printf("Erro a alocar memoria para str em symtypeToStr\n");
		exit(EXIT_FAILURE);
	}
	return NULL;
}
